# On démarre d'une image de base
FROM python:latest

# On copie les fichiers de notre app
COPY basic-flask-app ./app

#On change de répertoire de travail
WORKDIR app

# InSTALLATION DU VENV
RUN python3 -m venv venv

#Activation du venv
RUN . venv/bin/activate

#Installation de Flask
RUN pip install -U Flask

#On lance l'app au lancement du conteneur
CMD ["python", "routes.py"]

